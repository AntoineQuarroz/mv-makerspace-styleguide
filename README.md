# Valais Media Library - Digital & Creation space: Visual Identity
This repository contains the visual Identity of the _Digital & Creation space_ for the _Valais Media Library_. It establishes a common design language, provides reusable coded components, and outlines high level guidelines for content and accessibility.

# Logo
The logo uses the logo of the Valais Media Library, adding the elements of the Digital & Creation space while respecting the visual identity of the Valais Media Library. Each logo is available in `.svg`, `.jpg` and `.png`. You can find all the variants in the [`logo` directory](../tree/master/logo/).

## Regular
| **Color** | **Black & White** |
| :----------: | :------------:|
| ![Logo](/logo/dcs-sion-all.png) | Comming… |

## Variant
| **Color** | **Black & White** |
| :----------: | :------------:|
| ![Logo](/logo/dcs-sion-digital-workshop.png) | Comming… |

## Colors
| **Primary** | **Secondary** | **Tertiary**|
| :-------------:| :-------------: | :-------------:|
| ![color](/color/red.png) | ![color](/color/gris.png) | ![color](/color/red%20fonc%C3%A9.png)|
| **HEX** #DB0030      | **HEX** #ADAEAE      | **HEX** #9A042F
| **RGB** (219,0,48) | **RGB** (173,174,174) | **RGB** (154,4,47) |

## Typography
_Open Sans_ is a humanist sans serif typeface designed by Steve Matteson, Type Director of Ascender Corp. Open Sans was designed with an upright stress, open forms and a neutral, yet friendly appearance. It was optimized for print, web, and mobile interfaces, and has excellent legibility characteristics in its letterforms.

You can download it on [Font Squirrel](https://www.fontsquirrel.com/fonts/open-sans) or import it form [Google font](https://fonts.google.com/specimen/Open+Sans).

![alt text](https://upload.wikimedia.org/wikipedia/commons/0/0c/Open_Sans_sample.svg "Wikipedia User:Sbp [CC BY 3.0 (https://creativecommons.org/licenses/by/3.0)]")
